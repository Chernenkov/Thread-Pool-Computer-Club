import java.util.*
import java.util.concurrent.Executors

/**
 * Handles thread pool working. Uses [Executors.newFixedThreadPool()]
 * @param computersCount determines the number of computers in computer club
 * @param usersCount     determines the nubmer of users in computer club
 *
 * @author Alex Chernenkov
 */
class ComputerCaffee(computersCount: Int, usersCount: Int) {
    init{ startThreads(usersCount, computersCount)}

    /**
     * Creates a thread for each computer user. Creates pool for those threads.
     */
    private fun startThreads(usersCount: Int, computersCount: Int){
        val executor = Executors.newFixedThreadPool(computersCount)
        for (i in 0 until usersCount) {
            executor.submit(UserThread("Thread#$i", randomTime(15, 120).toLong()))
        }
        executor.shutdown()
    }

    /**
     * Determines a [random()] function for range of Ints.
     */
    private fun ClosedRange<Int>.random() = Random().nextInt(endInclusive - start) + start
    private fun randomTime(a: Int, b: Int): Int = (a .. b).random()
}
