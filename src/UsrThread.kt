/**
 * Describes one user's thread.
 *
 * @param timeOut determines allowed time to work by the computer for each user
 * @param name    name of thread is generated in [ComputerCaffee.startThreads()]
 *
 * @author Alex Chernenkov
 */
class UserThread (name: String, private var timeOut: Long) : Thread(name) {
    override fun run() {
        Thread.currentThread().name = name
        println("User ${currentThread().name} start work")
        try {
            Thread.sleep(timeOut * 100)
        } catch (e: InterruptedException) {
            e.printStackTrace()
        }
        println("User ${currentThread().name} end his work in $timeOut minutes")
    }
}
